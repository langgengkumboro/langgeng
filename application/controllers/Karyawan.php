<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait (manggil pertama kali scrip di jalanin)
		$this->load->model("Karyawan_models");
	}

	public function index()
	{
		$this->listKaryawan();


	}
	public function listKaryawan()
	{
		$data['data_Karyawan'] = $this->Karyawan_models->tampilDataKaryawan();
		$this->load->view('listKaryawan', $data);
	}

	public function detailKaryawan($nik)
	{
		$data['data_Karyawan'] =$this->Karyawan_models->detail($nik);
		$this->load->view('detailkaryawan', $data);
	}
	public function inputkaryawan()
	{
		$data['data_Karyawan'] = $this->Karyawan_models->tampilDataKaryawan();

		if (!empty($_REQUEST)){
			$m_karyawan = $this->Karyawan_models;
			$m_karyawan->save();
			redirect("Karyawan/index", "refresh");
		}


		$this->load->view('inputkaryawan',$data);
	}
	public function editkaryawan($nik)
	{	
		$data['detail_karyawan']	= $this->Karyawan_models->detail($nik);
		
		if (!empty($_REQUEST)) {
				$m_karyawan = $this->Karyawan_models;
				$m_karyawan->update($nik);
				redirect("Karyawan/index", "refresh");	
			}
		
		$this->load->view('editkaryawan', $data);	
	}
	public function delete($nik)
	{
		$m_karyawan = $this->Karyawan_models;
		$m_karyawan->delete($nik);	
		redirect("Karyawan/index", "refresh");	
	}

	
}
