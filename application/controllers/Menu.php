<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait (manggil pertama kali scrip di jalanin)
		$this->load->model("Menu_models");
	}

	public function index()
	{
		$this->listmenu();


	}
	public function listmenu()
	{
		$data['data_menu'] = $this->Menu_models->tampilDataMenu();
		$this->load->view('listmenu', $data);
	}

	public function detailmenu($kode_menu)
	{
		$data['data_menu'] =$this->Menu_models->detailmenu($kode_menu);
		$this->load->view('detailmenu', $data);
	}
	public function inputmenu()
	{
		$data['data_menu'] = $this->Menu_models->tampilDataMenu();

		if (!empty($_REQUEST)){
			$m_menu = $this->Menu_models;
			$m_menu->save();
			redirect("Menu/index", "refresh");
		}
		$this->load->view('inputmenu',$data);
	}
	
	public function editmenu($kode_menu)
	{	
		$data['data_menu']	= $this->Menu_models->detailmenu($kode_menu);
		
		if (!empty($_REQUEST)) {
				$m_menu = $this->Menu_models;
				$m_menu->update($kode_menu);
				redirect("Menu/index", "refresh");	
			}
		
		$this->load->view('editmenu', $data);	
	}

	public function delete($kode_menu)
	{
		$m_menu = $this->Menu_models;
		$m_menu->delete($kode_menu);	
		redirect("Menu/index", "refresh");	
	}	
}
